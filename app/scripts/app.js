'use strict';

/**
 * @ngdoc overview
 * @name carClientApp
 * @description
 * # carClientApp
 *
 * Main module of the application.
 */
var app = angular
    .module('carClientApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ]);

app
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/make', {
                templateUrl: 'views/make.html',
                controller: 'makeCtrl',
                controllerAs: 'make'
            })
            .otherwise({
                redirectTo: '/'
            });
    });