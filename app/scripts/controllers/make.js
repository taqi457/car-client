'use strict';

/**
 * @ngdoc function
 * @name carClientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the carClientApp
 */
angular.module('carClientApp')
    .controller('makeCtrl', ['$scope', '$rootScope', '$location', 'carService', '$routeParams',
            function ($scope, $rootScope, $location, carService, $routeParams) {
            $scope.data = {
                brand: '',
                selectedCar: {},
                carList: null
            };

            var getCars = function (brand) {
                console.log(brand);
                $scope.data.brand = brand;
                $scope.data.carList = carService.getCars(brand).models;
            }
            var selectCar = function (carObject) {
                $scope.data.selectedCar = carObject;
                carService.getImages($scope.data.brand + carObject.name).then(function (mediaLink) {
                    $scope.data.selectedCar.image = mediaLink;
                });
            }

            $scope.methods = {
                selectCar: selectCar
            };


            getCars($routeParams.make);
}]);
